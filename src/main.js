import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import router from './routes/index';

import 'element-plus/theme-chalk/index.css';
import App from './App.vue'

const app = createApp(App)

// Use Element Plus
app.use(ElementPlus)
app.use(router)


app.mount('#app')

import { createRouter, createWebHashHistory } from 'vue-router';
import DashBoard from '../views/DashBoard';
import HomeOne from '../components/homepage/HomeOne.vue';
import PropertyLocation from '../components/PropertyLocation.vue';
import PropertyAddress from '../components/PropertyAddress.vue';
import RentalDetails from '../components/RentalDetails.vue';
import PropertyDetails from '../components/PropertyDetails.vue';
import LandlordInfo from '../components/LandlordInfo.vue';
import TenantInfo from '../components/TenantInfo.vue';
import GuarantorInfo from '../components/GuarantorInfo.vue';
import RentInfo from '../components/RentInfo.vue';
import propertyUse from '../components/propertyUse.vue';
import DepositDetails from '../components/DepositDetails.vue';
import NoticeInfo from '../components/NoticeInfo.vue';
import  AdditionalCharges from '../components/AdditionalCharges.vue';
import ImprovementInfo from '../components/ImprovementInfo.vue';
import ContactDetails from '../components/ContactDetails.vue';
import MiscellaneousInfo from '../components/MiscellaneousInfo.vue';
import AdditionalClauses from '../components/AdditionalClauses.vue';
import SigningDetails from '../components/SigningDetails.vue';
import DownloadInfo from '../components/DownloadInfo.vue';

const routes=[
      {
        path: '/',
        component: DashBoard,
        name: 'DashBoard'
      },
      {
        path: '/HomeOne',
        component: HomeOne,
        name: 'HomeOne',
      },
      {
        path: '/PropertyLocation',
        component: PropertyLocation,
        name: 'PropertyLocation',
      },
      {
        path: '/RentalDetails',
        component: RentalDetails,
        name: 'RentalDetails',
      },
      {
        path: '/PropertyAddress',
        component: PropertyAddress,
        name: 'PropertyAddress',
      },
      {
        path: '/PropertyDetails',
        component: PropertyDetails,
        name: 'PropertyDetails',
      },
      {
        path: '/LandlordInfo',
        component: LandlordInfo,
        name: 'LandlordInfo',

      },
      {
        path: '/TenantInfo',
        component: TenantInfo,
        name: 'TenantInfo',

      },
      {
        path: '/GuarantorInfo',
        component: GuarantorInfo,
        name: 'GuarantorInfo',

      },
      {
        path: '/RentInfo',
        component: RentInfo,
        name: 'RentInfo',

      },
      {
        path: '/propertyUse',
        component: propertyUse,
        name: 'propertyUse',
      },
      {
        path: '/DepositDetails',
        component: DepositDetails,
        name: 'DepositDetails',
      },
      {
        path: '/NoticeInfo',
        component: NoticeInfo,
        name: 'NoticeInfo',
      },
      {
        path: '/AdditionalCharges',
        component: AdditionalCharges,
        name: 'AdditionalCharges',
      },
      {
        path: '/ImprovementInfo',
        component: ImprovementInfo,
        name: 'ImprovementInfo',

      },
      {
        path: '/ContactDetails',
        component: ContactDetails,
        name: 'ContactDetails',

      },
      {
        path: '/MiscellaneousInfo',
        component: MiscellaneousInfo,
        name: 'MiscellaneousInfo',

      },
      {
        path: '/AdditionalClauses',
        component: AdditionalClauses,
        name: 'AdditionalClauses',

      },
      {
        path: '/SigningDetails',
        component: SigningDetails,
        name: 'SigningDetails',

      },
      {
        path: '/DownloadInfo',
        component: DownloadInfo,
        name: 'DownloadInfo',

      },
    
    
]
const router = createRouter({
    history: createWebHashHistory(),
    routes
  });
  
  export default router;